# basically
# https://www.tensorflow.org/beta/tutorials/keras/overfit_and_underfit

from __future__ import absolute_import, division, print_function

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt

print(tf.__version__)
print(tf.keras.__version__)

NUM_WORDS = 10000

# load imdb reviews dataset, each entry is a array of indexes
(train_data, train_labels), (test_data, test_labels) = keras.datasets.imdb.load_data(num_words=NUM_WORDS)

# Multi-hot-encoding our lists means turning them into vectors of 0s and 1s. Concretely, this would mean for instance
# turning the sequence [3, 5] into a 10,000-dimensional vector that would be all-zeros except for indices 3 and 5,
# which would be ones.
def multi_hot_sequences(sequences, dimension):
    # Create an all-zero matrix of shape (len(sequences), dimension)
    results = np.zeros((len(sequences), dimension))
    for i, word_indices in enumerate(sequences):
        results[i, word_indices] = 1.0  # set specific indices of results[i] to 1s
    return results


train_data = multi_hot_sequences(train_data, dimension=NUM_WORDS)
test_data = multi_hot_sequences(test_data, dimension=NUM_WORDS)

# display first encoded data..
plt.plot(train_data[0])

# create model
baseline_model = keras.Sequential()
# `input_shape` is only required here so that `.summary` works.
baseline_model.add(keras.layers.Dense(16, activation='relu', input_shape=(NUM_WORDS,)))
baseline_model.add(keras.layers.Dense(16, activation='relu'))
baseline_model.add(keras.layers.Dense(1, activation='sigmoid'))

baseline_model.compile(optimizer='adam',
                       loss='binary_crossentropy',
                       metrics=['accuracy', 'binary_crossentropy'])

baseline_model.summary()

baseline_history = baseline_model.fit(train_data,
                                      train_labels,
                                      epochs=20,
                                      batch_size=512,
                                      validation_data=(test_data, test_labels),
                                      verbose=2)

# create model
smaller_model = keras.Sequential()
smaller_model.add(keras.layers.Dense(4, activation='relu', input_shape=(NUM_WORDS,)))
smaller_model.add(keras.layers.Dense(4, activation='relu'))
smaller_model.add(keras.layers.Dense(1, activation='sigmoid'))

smaller_model.compile(optimizer='adam',
                      loss='binary_crossentropy',
                      metrics=['accuracy', 'binary_crossentropy'])

smaller_model.summary()

smaller_history = smaller_model.fit(train_data,
                                    train_labels,
                                    epochs=20,
                                    batch_size=512,
                                    validation_data=(test_data, test_labels),
                                    verbose=2)

# create model
bigger_model = keras.Sequential()
bigger_model.add(keras.layers.Dense(512, activation='relu', input_shape=(NUM_WORDS,)))
bigger_model.add(keras.layers.Dense(512, activation='relu'))
bigger_model.add(keras.layers.Dense(1, activation='sigmoid'))

bigger_model.compile(optimizer='adam',
                     loss='binary_crossentropy',
                     metrics=['accuracy','binary_crossentropy'])

bigger_model.summary()

bigger_history = bigger_model.fit(train_data, train_labels,
                                  epochs=20,
                                  batch_size=512,
                                  validation_data=(test_data, test_labels),
                                  verbose=2)

def plot_history(histories, key='binary_crossentropy'):
  plt.figure(figsize=(16,10))

  for name, history in histories:
    val = plt.plot(history.epoch, history.history['val_'+key],
                   '--', label=name.title()+' Val')
    plt.plot(history.epoch, history.history[key], color=val[0].get_color(),
             label=name.title()+' Train')

  plt.xlabel('Epochs')
  plt.ylabel(key.replace('_',' ').title())
  plt.legend()

  plt.xlim([0, max(history.epoch)])


plot_history([('baseline', baseline_history),
              ('smaller', smaller_history),
              ('bigger', bigger_history)])

# Next test
####

# create model
l2_model = keras.models.Sequential()
# L2 regularization, where the cost added is proportional to the square of the value of the weights
# coefficients (i.e. to what is called the squared "L2 norm" of the weights).
# L2 regularization is also called weight decay in the context of neural networks.
# Don't let the different name confuse you: weight decay is mathematically the exact same as L2 regularization.
l2_model.add(keras.layers.Dense(16, kernel_regularizer=keras.regularizers.l2(0.001), activation='relu', input_shape=(NUM_WORDS,)))
l2_model.add(keras.layers.Dense(16, kernel_regularizer=keras.regularizers.l2(0.001), activation='relu'))
l2_model.add(keras.layers.Dense(1, activation='sigmoid'))

l2_model.compile(optimizer='adam',
                 loss='binary_crossentropy',
                 metrics=['accuracy', 'binary_crossentropy'])

l2_model_history = l2_model.fit(train_data, train_labels,
                                epochs=20,
                                batch_size=512,
                                validation_data=(test_data, test_labels),
                                verbose=2)

plot_history([('baseline', baseline_history),
              ('l2', l2_model_history)])

# create model
dpt_model = keras.models.Sequential()
l2_model.add(keras.layers.Dense(16, activation='relu', input_shape=(NUM_WORDS,)))
# Dropout is one of the most effective and most commonly used regularization techniques for neural networks,
# developed by Hinton and his students at the University of Toronto. Dropout, applied to a layer,
# consists of randomly "dropping out" (i.e. set to zero) a number of output features of the layer during training.
l2_model.add(keras.layers.Dropout(0.5))
l2_model.add(keras.layers.Dense(16, activation='relu'))
l2_model.add(keras.layers.Dropout(0.5))
l2_model.add(keras.layers.Dense(1, activation='sigmoid'))

dpt_model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['accuracy','binary_crossentropy'])

dpt_model_history = dpt_model.fit(train_data, train_labels,
                                  epochs=20,
                                  batch_size=512,
                                  validation_data=(test_data, test_labels),
                                  verbose=2)

plot_history([('baseline', baseline_history),
              ('dropout', dpt_model_history)])

