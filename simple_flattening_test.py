
# Test to se what "flattening of multidimensional array", does.. honestly, there is no such thing as multidimensional arrays.. but .. what ever..

import numpy as np
from tensorflow import keras

# create model
model = keras.Sequential()
# & set layers
model.add(keras.layers.Flatten(input_shape=(3, 2, 4)))

# Define a model consisting only of the Flatten operation
X = np.arange(0, 24).reshape(1, 3, 2, 4)
print(X)
#[[[[ 0  1  2  3]
#   [ 4  5  6  7]]
#
#  [[ 8  9 10 11]
#   [12 13 14 15]]
#
#  [[16 17 18 19]
#   [20 21 22 23]]]]
predictions = model.predict(X)
print(predictions)
#array([[  0.,   1.,   2.,   3.,   4.,   5.,   6.,   7.,   8.,   9.,  10.,
#         11.,  12.,  13.,  14.,  15.,  16.,  17.,  18.,  19.,  20.,  21.,
#         22.,  23.]], dtype=float32)

print('Save model')
model.save('simple_flattening_test.h5', overwrite=True)