# basically
# https://www.tensorflow.org/tutorials/keras/basic_classification
# https://www.tensorflow.org/beta/tutorials/quickstart/beginner
# https://www.tensorflow.org/beta/tutorials/images/hub_with_keras

from __future__ import absolute_import, division, print_function

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt
from math import floor

print(tf.__version__)
print(tf.keras.__version__)

# access the Fashion MNIST dataset directly from TensorFlow, just import and load the data
fashion_mnist = keras.datasets.fashion_mnist

item_types = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

# get black and white images..
#  a) training images and labels
#  b) test/verification images and labels to evaluate how accurately the network learned
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

# preprocess the images, from [0,255] space to normalized space.
train_images = train_images / 255.0
test_images = test_images / 255.0

# create model & set neural network layers
model = keras.Sequential([
    # 1st layer. Take 2D 28x28 image, and transform it into 1D 784 array.
    keras.layers.Flatten(input_shape=(28, 28)),

    # 2nd layer. densely-connected, neural layer. 128 nodes (so the 784 is squeezed into 128 weights)
    keras.layers.Dense(128, activation=tf.nn.relu),
    # 3rd layer. densely-connected neural layer. 10-node softmax layer—this returns an array of 10 probability scores that sum to 1.
    # Each node contains a score that indicates the probability that the current image belongs to one of the 10 classes.
    keras.layers.Dense(10, activation=tf.nn.softmax)
])

# configure learning process by calling the compile with configuration params
model.compile(
    # This is how the model is updated based on the data it sees and its loss function
    optimizer='adam',
    # This measures how accurate the model is during training.
    loss='sparse_categorical_crossentropy',
    # Used to monitor the training and testing steps.
    metrics=['accuracy'])

# Train the model
model.fit(train_images, train_labels, epochs=5)

# test the model against the evaluation data
test_loss, test_acc = model.evaluate(test_images, test_labels)

print('Test loss:', test_loss)
print('Test accuracy:', test_acc)

# test whether the classification works.. lets take one image, and let the model predict what is in it.
item = test_images[0:1]
predictions = model.predict(item)[0]
index = np.argmax(predictions)

print('The image provided is a:', item_types[index])

plt.figure()
plt.imshow(item[0])
plt.colorbar()
plt.grid(False)
str = "Item is '{0}', {1}% certainty.".format(item_types[index], floor(predictions[index] * 100.0))
plt.xlabel(str)
plt.show()
