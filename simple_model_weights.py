# basically
# https://www.tensorflow.org/beta/tutorials/images/hub_with_keras

from __future__ import absolute_import, division, print_function

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt
from math import floor

print(tf.__version__)
print(tf.keras.__version__)

# access the Fashion MNIST dataset directly from TensorFlow, just import and load the data
fashion_mnist = keras.datasets.fashion_mnist

item_types = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

# get black and white images..
#  a) training images and labels
#  b) test/verification images and labels to evaluate how accurately the network learned
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

# preprocess the images, from [0,255] space to normalized space.
train_images = train_images / 255.0
test_images = test_images / 255.0

model = keras.models.load_model('01_basic.h5')

# test whether the classification works.. lets take one image, and let the model predict what is in it.
item = test_images[0:1]
predictions = model.predict(item)[0]
index = np.argmax(predictions)

print(predictions)

# write test image to file
newFile = open("image.blob", "wb")
newFile.write(item)

print('The image provided is a:', item_types[index])

# create model
model2 = keras.Sequential()
# & set layers
# 1st layer. Take 2D 28x28 image, and transform it into 1D 784 array.
model2.add(keras.layers.Flatten(input_shape=(28, 28)))

# 2nd layer. densely-connected, neural layer. 128 nodes (so the 784 is squeezed into 128 weights)
model2.add(keras.layers.Dense(128, activation=tf.nn.relu))

# 3rd layer. densely-connected neural layer. 10-node softmax layer—this returns an array of 10 probability scores that sum to 1.
# Each node contains a score that indicates the probability that the current image belongs to one of the 10 classes.
model2.add(keras.layers.Dense(10, activation=tf.nn.softmax))

# configure learning process by calling the compile with configuration params
model2.compile(
    # This is how the model is updated based on the data it sees and its loss function
    optimizer='adam',
    # This measures how accurate the model is during training.
    loss='sparse_categorical_crossentropy',
    # Used to monitor the training and testing steps.
    metrics=['accuracy'])

weights = model.layers[2].get_weights()

model2.layers[0].set_weights(model.layers[0].get_weights())
model2.layers[1].set_weights(model.layers[1].get_weights())
model2.layers[2].set_weights(model.layers[2].get_weights())

for index in range(len(weights)):
    val = weights[index]
    file = open("atan_{0}.txt".format(index), "w")
    for item in val:
        if isinstance(item, np.ndarray):
            line = []
            for arr in item:
                line.append("{}".format(arr))
            file.write(",\t".join(line))
        else:
            file.write("{}".format(item))
        file.write(",\n")
    file.close()

print("HOOO")

predictions = model2.predict(item)[0]

print(predictions)
