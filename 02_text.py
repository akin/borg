# basically
# https://www.tensorflow.org/beta/tutorials/keras/basic_text_classification

from __future__ import absolute_import, division, print_function

# TensorFlow and tf.keras
import tensorflow as tf
from tensorflow import keras

# Helper libraries
import numpy as np
import matplotlib.pyplot as plt
from math import floor

print(tf.__version__)
print(tf.keras.__version__)

# access the imdb MNIST dataset directly from TensorFlow, just import and load the data
imdb = keras.datasets.imdb

vocab_size = 10000

# get text reviews..
#  a) training data and labels
#  b) test/verification data and labels to evaluate how accurately the network learned
#
# The argument num_words=10000 keeps the top 10,000 most frequently occurring words in the training data.
# The rare words are discarded to keep the size of the data manageable.
(train_data, train_labels), (test_data, test_labels) = imdb.load_data(num_words=vocab_size)

# The text of reviews have been converted to integers, where each integer represents a specific word in a dictionary.
# A dictionary mapping words to an integer index
word_index = imdb.get_word_index()

# The first indices are reserved
word_index = {k:(v+3) for k,v in word_index.items()}
word_index["<PADDING>"] = 0
word_index["<START>"] = 1
word_index["<UNKNOWN>"] = 2
word_index["<UNUSED>"] = 3

reverse_word_index = dict([(value, key) for (key, value) in word_index.items()])

def decode_review(text):
    return ' '.join([reverse_word_index.get(i, '?') for i in text])


print("Training entries: {}, labels: {}".format(len(train_data), len(train_labels)))

# preprocess the data, normalize data to be 256 length reviews
# (which is quite bullshit approach, why the hell would you want to create a nn
# from a natural language text)
train_data = keras.preprocessing.sequence.pad_sequences(train_data,
                                                        value=word_index["<PADDING>"],
                                                        padding='post',
                                                        maxlen=256)

test_data = keras.preprocessing.sequence.pad_sequences(test_data,
                                                       value=word_index["<PADDING>"],
                                                       padding='post',
                                                       maxlen=256)

model = keras.Sequential()
# The first layer is an Embedding layer. This layer takes the integer-encoded vocabulary and looks up the embedding
# vector for each word-index. These vectors are learned as the model trains. The vectors add a dimension to the
# output array. The resulting dimensions are: (batch, sequence, embedding)
model.add(keras.layers.Embedding(vocab_size, 16))
# Next, a GlobalAveragePooling1D layer returns a fixed-length output vector for each example by averaging over the
# sequence dimension. This allows the model to handle input of variable length, in the simplest way possible.
model.add(keras.layers.GlobalAveragePooling1D())
# The fixed-length output vector is piped through a fully-connected (Dense) layer with 16 hidden units.
model.add(keras.layers.Dense(16, activation='relu'))
# The last layer is densely connected with a single output node.
# Using the sigmoid activation function, this value is a float between 0 and 1,
# representing a probability, or confidence level.
model.add(keras.layers.Dense(1, activation='sigmoid'))

model.summary()

# configure learning process by calling the compile with configuration params
model.compile(
    # This is how the model is updated based on the data it sees and its loss function
    optimizer='adam',
    # This measures how accurate the model is during training.
    loss='binary_crossentropy',
    # Used to monitor the training and testing steps.
    metrics=['accuracy'])

# Partial data, to teach the network
x_val = train_data[:10000]
partial_x_train = train_data[10000:]

y_val = train_labels[:10000]
partial_y_train = train_labels[10000:]

# Train the model
history = model.fit(partial_x_train,
                    partial_y_train,
                    epochs=40,
                    batch_size=512,
                    validation_data=(x_val, y_val),
                    verbose=1)

# test the model against the evaluation data
results = model.evaluate(test_data, test_labels)

print(results)

history_dict = history.history
history_dict.keys()

# test whether the classification works..
history_dict = history.history
history_dict.keys()

acc = history_dict['accuracy']
val_acc = history_dict['val_accuracy']
loss = history_dict['loss']
val_loss = history_dict['val_loss']

epochs = range(1, len(acc) + 1)

# "bo" is for "blue dot"
plt.plot(epochs, loss, 'bo', label='Training loss')
# b is for "solid blue line"
plt.plot(epochs, val_loss, 'b', label='Validation loss')
plt.title('Training and validation loss')
plt.xlabel('Epochs')
plt.ylabel('Loss')
plt.legend()

plt.show()

plt.clf()   # clear figure

plt.plot(epochs, acc, 'bo', label='Training acc')
plt.plot(epochs, val_acc, 'b', label='Validation acc')
plt.title('Training and validation accuracy')
plt.xlabel('Epochs')
plt.ylabel('Accuracy')
plt.legend()

plt.show()
